var screen_height = document.getElementById("main").clientHeight;
var screen_width = document.getElementById("main").clientWidth;

var screen_left = 100;
var screen_right = screen_width - 100 - 30; //100 for the div, 30 for invader width

var movedown = false;
var moveright = true;

var player_missiles_motion_interval = [];
var player_missiles_collision_interval = [];

var invader_missiles = [];
var invader_missiles_motion_interval = [];
var invader_missiles_collision_interval = [];

var firing_inverval = null;

var invaders_move_timer_interval = 14;  // yeah, I know these are a bit random, I just played around with them a bit until I go to this
var float_invaders_move_timer_interval = 14.0;

var invader_missile_timer_interval = 7;
var invader_missile_collision_timer_interval = 17;
var player_missile_timer_interval = 5;
var player_missile_collision_timer_interval = 7;
var invaders_firing_timer_interval = 150;

var invaders_motion_interval = [];
var invaders = [];
var bases = [];
var ship;

var player_lives = 3;
var game_over = false;
var has_bases = true;

const lose = 0;
const win = 1;

var invader_count = 50;
var invaders_remaining = 50;
var max_invader_missiles = 20;

var invader_img_counters = []; // counts 0-1-0-1 to switch images
var invader_img_timers = []; //when this is 3 we change the image

var img_dir = "images/";

var stylesheet;

function invaders_game(){

	stylesheet = document.styleSheets[0];
	
	setup_game();
	
	for(c=0; c<invader_count; c++){
		id = invaders[c].id
		start_invaders_timers(id);	
	}
	start_firing_timer();
	start_explosions_timer();
	//autofire();
}

function start_invaders_timers(id) {
 	var num = id.substring(1);
 	var index = parseInt(num);
	
 	var invader = document.getElementById(id);
    var speed = 1;
    var current_left = parseInt(invader.style.left, 10);
   
    invaders_motion_interval[index] = setInterval(function() {
		if (moveright){
			current_left += speed;
		}else{
			current_left -= speed;
		}
		
		if (current_left >= screen_right) {
			current_left = screen_right;
			movedown = true;
			moveright = false;
		}
	
		if (current_left <= screen_left) {
			current_left = screen_left;
			movedown = true;
			moveright = true;
		}

		if (movedown){
			move_down();
		}

		invader_img_timers[index]++;
		if(invader_img_timers[index] == 8){
			invader_img_timers[index] = 0;
			
			invader_img_counters[index]++;
			if(invader_img_counters[index] == 2){
				invader_img_counters[index] = 0;
			}

			if(!invaders[index].exploding){
				if (index < 10 ){
					invaders[index].img = img_dir + "top_invader" + invader_img_counters[index] + ".png";
					invader.setAttribute("src", invaders[index].img);
				}
				else if (index >= 10 && index < 30){
					invaders[index].img = img_dir + "middle_invader" + invader_img_counters[index] + ".png";
					invader.setAttribute("src", invaders[index].img);
				}
				else{
					invaders[index].img = img_dir + "bottom_invader" + invader_img_counters[index] + ".png";
					invader.setAttribute("src", invaders[index].img);
				}
			}
		}
		invader.style.left = current_left + "px";
	}, invaders_move_timer_interval);
}
	
function move_down(){
	movedown = false;
		
	for(c=0; c < invaders.length; c++){
		var id = "i" + c;
		if(invaders[c].alive){
			invader = document.getElementById(id);
			var top = parseInt(invader.style.top);
			top += 20;
			invader.style.top = top + "px";

			if (top >= 430){
				has_bases = false;
				remove_bases();
			}
			
			if(top >= 600){
				clearInterval(invaders_motion_interval [c]);
			}	
		}
	}
}

function autofire(){
	auto_firing_interval = setInterval(function() {
		missile = auto_fire_invader_missile();
		move_invader_missile(missile, 0);
	}, 2000);
}

function auto_fire_invader_missile(){
	var missile = document.createElement("DIV");
	img = img_dir + "inv_missile.png"
	missile.style.backgroundImage = "url(" + img + ")";
	missile.style.zIndex = "10";
	 
	id = "invader_missile0";
	missile.setAttribute("id", id);
	missile.setAttribute("class", "missile");
	
    var left = 298;
	missile.style.left = left +"px";
	
	var top = 400;
	missile.style.top = top + "px"
	
	document.getElementById("main").appendChild(missile);
	
	return missile;
}

function start_firing_timer(){
	
	firing_interval = setInterval(function() {
		var to_fire = Math.random();
		var can_fire = false;
		var missile_index;
		var inv_to_fire;

		if(to_fire >0.7){		
			for(missile_index=0; missile_index < invader_missiles.length; missile_index++){
				if (invader_missiles[missile_index] == false){
					can_fire = true;
					invader_missiles[missile_index] = true;
					break;
				}
			}
		}
			
		if (can_fire){
			found = false;
			while (!found){
				inv_to_fire = Math.floor(Math.random() * invader_count);
				if (invaders[inv_to_fire].alive){
					found = true;
				}
			}
			
			if(found){
				invader_id = invaders[inv_to_fire].id;
				missile = fire_invader_missile(invader_id, missile_index);
				move_invader_missile(missile, missile_index);
			}
		}
	}, invaders_firing_timer_interval);
}

function start_explosions_timer(){

	explosions_interval = setInterval(function() {
		len = invaders.length;
		for(c=0;c<len;c++){
		
			if (invaders[c].exploding){
				invaders[c].explosion_timer--;
				//console.log("invaders[c].explosion_timer at expl timer = ");
				//console.log(invaders[c].explosion_timer);
				//console.log("invaders[c] =");
				//console.log(invaders[c]);
				if(invaders[c].explosion_timer == 0){
					invaders[c].exploding = false;
					invaders[c].alive = false;
				
					main = document.getElementById("main");					
					child = document.getElementById(invaders[c].id);
					//console.log("child elem at expl timer =");
					//console.log(child);
					main.removeChild(child);
				
					clearInterval(invaders_motion_interval [c]);
				}
			}
		}
	
		if(has_bases){
			len = bases.length;
			
			for(c=0;c<len;c++){
				explosions = bases[c].explosions;
				e_len = explosions.length;
				if (e_len > 0 ){
					for(d=0;d<e_len;d++){
						explosions[d][0]--;
									
						if(explosions[d][0] <= 0){
							main = document.getElementById("main");
							child = document.getElementById(explosions[d][1]);
							main.removeChild(child);
							explosions.splice(d, 1);
							e_len--;
						}
					}
				}
			}
		}
			
		if (ship.exploding){
			ship.explosion_timer--;
			if (ship.explosion_timer == 0){
				if(ship.explosion_image_counter == 0){
					ship.explosion_image_counter++;
					img = img_dir + "ship_exp1.png";	
					s = document.getElementById("ship");
					s.style.backgroundImage = "url(" + img + ")";
					ship.explosion_timer = 5;		
				}
				if(ship.explosion_image_counter == 1){
					ship.exploding = false;
					ship.explosion_timer = 5;
					ship.explosion_image_counter = 0;
				}
			}
		}
	}, 20);
}

function fire_invader_missile(invader_id, index){
	var missile = document.createElement("img");
	inv = document.getElementById(invader_id);
	
	img = img_dir + "inv_missile.png"
	missile.setAttribute("id", index);
	missile.setAttribute("class", "missile");
	missile.setAttribute("src", img);
	
	missile.style.zIndex = "10";
	
    var left = parseInt(inv.style.left) + 15;
	missile.style.left = left +"px";
	
	var top = parseInt(inv.style.top) + 20;
	missile.style.top = top + "px"
	
	document.getElementById("main").appendChild(missile);
	
	return missile;
}

function move_invader_missile(missile, index){
	var current_pos = parseInt(missile.style.top, 10);		
	var speed = 1
	
	invader_missiles_motion_interval[index] = setInterval(function() {
		current_pos += speed;		
		missile.style.top = current_pos+"px";
		
		if (current_pos >= screen_height - 25 ) {
			remove_invader_missile(missile);
		}
	}, invader_missile_timer_interval);
	
	invader_missiles_collision_interval[index] = setInterval(function() {
		collision_check_ship(missile);
		if(has_bases){
			collision_check_invader_missile_bases(missile);
		}
	}, invader_missile_collision_timer_interval);
}	

function remove_invader_missile(missile){
	var index = parseInt(missile.id);

	invader_missiles[index] = false;
	clearInterval(invader_missiles_motion_interval[index]);
	clearInterval(invader_missiles_collision_interval[index]);
	
	main = document.getElementById("main");
	m = document.getElementById(missile.id)
	main.removeChild(m);
}

function collision_check_ship(missile){
	var missile_rect =  missile.getBoundingClientRect();
	var ship = document.getElementById("ship");
	var ship_rect = ship.getBoundingClientRect();

	if (ship_rect.left < missile_rect.right && ship_rect.right > missile_rect.left && ship_rect.top < missile_rect.bottom && ship_rect.bottom > missile_rect.top ){
		player_lives--;
		remove_invader_missile(missile);
		ship.exploding = true;
		img = img_dir + "ship_exp0.png";
		s = document.getElementById("ship");
		s.setAttribute("src", img);		
		
		if (player_lives == 0){
			game_over == true;
			show_game_over(lose)
		}
		reset()
	}	
} 
	
function collision_check_invader_missile_bases(missile){
	var missile_rect =  missile.getBoundingClientRect();
	
	for(c=0;c<4;c++){
		id = "base" + c;
		base = document.getElementById(id);
		base_rect = base.getBoundingClientRect();

		if (base_rect.left < missile_rect.right && base_rect.right > missile_rect.left && base_rect.top < missile_rect.bottom && base_rect.bottom > missile_rect.top ){
			overlap = check_invader_missile_overlap(missile_rect, base, base_rect);
	
			if (overlap){
				remove_invader_missile(missile);
			}
		}
	}
} 

function check_invader_missile_overlap(missile_rect, base, base_rect){
	var id = base.id.substring(4);
 	var index = parseInt(id);

	var x_overlap, y_overlap, sx, sy;
	
	if(missile_rect.left < base_rect.left){
		x_overlap = missile_rect.right - base_rect.left;
		sx = 0;
	}
	
	if(missile_rect.left >= base_rect.left && missile_rect.right <= base_rect.right){
		x_overlap = missile_rect.width;
		sx = missile_rect.left - base_rect.left;
	}
	
	if (missile_rect.right > base_rect.right){
		x_overlap = base_rect.right - missile_rect.left;
		sx = base_rect.right - x_overlap;
	}

	y_overlap = missile_rect.bottom - base_rect.top;
	
	if(y_overlap < 2){
		sy = 0;
	}else{
		sy = y_overlap - 2;
		y_overlap = 2;
	}
	
	var context = base.getContext("2d");
	var image_data = context.getImageData(sx, sy, x_overlap, y_overlap);
	
	let collision = false;
	var x, y;
	
	for(x=0; x<x_overlap;x++){
		for(y=0; y< y_overlap;y++){
			var colorIndices = getColorIndicesForCoord(x, y, x_overlap);

			var redIndex = colorIndices[0];
			var greenIndex = colorIndices[1];
			var blueIndex = colorIndices[2];
				
			var redForCoord = image_data.data[redIndex];
			var greenForCoord = image_data.data[greenIndex];
			var blueForCoord = image_data.data[blueIndex];
			
			if (redForCoord !=0 && greenForCoord != 0 && blueForCoord != 0){
				collision = true;
			}
		}
	}
	
	if (collision){
		context.putImageData(image_data, sx, sy);
		
		exp_screen_loc_top = missile_rect.bottom - 11; // 11 is height of expl png img
		exp_screen_loc_left = missile_rect.left - 8; // 8 is about half width of expl png img
				
		var expl = document.createElement("img");
		var img = img_dir + "base_exp.png"
		expl.setAttribute("src", img);
		
		expl.setAttribute("class", "base_exp");
		expl.setAttribute("id", "base_exp" + bases[index].explosions_counter);
		
		expl.style.top = exp_screen_loc_top + "px";
		expl.style.left = exp_screen_loc_left + "px";
		expl.style.zIndex = "10";
		
		bases[index].explosions_counter++;
		var explosion = [5, expl.id];
		
		bases[index].explosions.push(explosion);
		
		document.getElementById("main").appendChild(expl);	
					
		var img_data = context.createImageData(10,10);
		context.putImageData(img_data, sx, sy);
		return true;
	}
	return false;
}

function getColorIndicesForCoord(x, y, width) {
  var red = y * (width * 4) + x * 4;
  return [red, red + 1, red + 2, red + 3];
}

function fire_player_missile(){
	if(ship.missile_count < 3){
		//console.log("ship.missile_count = " + ship.missile_count);		
		let missile = document.createElement("img");
		img = img_dir + "ship_missile.png"

		missile.setAttribute("id", ship.missile_count);
		missile.setAttribute("class", "missile");
		missile.setAttribute("src", img);
			
		var left_str = getCssProperty("ship", "left");
    	var left = parseInt(left_str, 10) + 15;
		missile.style.left = left +"px";
	
		var top = screen_height - 20;
		missile.style.top = top + "px"
		document.getElementById("main").appendChild(missile);

		ship.missile_count++;
		move_player_missile(missile);
	}
}

function move_player_missile(missile){
 	var index = parseInt(missile.id);
	
	var current_pos = parseInt(missile.style.top, 10);		
	var speed = 1
		
	player_missiles_motion_interval[index] = setInterval(function() {
		current_pos -= speed;
		missile.style.top = current_pos+"px";
	
		if (current_pos <= 0) {
			remove_player_missile(missile)
		}		
	}, player_missile_timer_interval);

	player_missiles_collision_interval[index] = setInterval(function() {
		collision_check_invaders(missile);
	 	if(has_bases){
	 		collision_check_player_missile_bases(missile);		
	 	}
	}, player_missile_collision_timer_interval);
}	

function remove_player_missile(missile){
	//console.log("missile at remove missile = ");
	//console.log(missile);
	//console.log(missile.id)

 	var index = parseInt(missile.id);
	
	clearInterval(player_missiles_motion_interval[index]);
	clearInterval(player_missiles_collision_interval[index]);
	
	main = document.getElementById("main");
	child = document.getElementById(missile.id);
	main.removeChild(child);
	ship.missile_count--;
}

function collision_check_invaders(missile){
	
	var missile_rect =  missile.getBoundingClientRect();
	
	for(c=0;c<invaders.length;c++){
		var id = "i" +c;
		
		if (invaders[c].alive){									
			inv = document.getElementById(id);
			inv_rect = inv.getBoundingClientRect();

			if (inv_rect.left < missile_rect.right && inv_rect.right > missile_rect.left && inv_rect.top < missile_rect.bottom && inv_rect.bottom > missile_rect.top ){
				remove_player_missile(missile);
				invaders[c].exploding = true;
				img = img_dir + "inv_exp.png";
				inv.setAttribute("src", img);
				
				invaders_remaining--;
				float_invaders_move_timer_interval -= 0.2;
				invaders_move_timer_interval = Math.round(float_invaders_move_timer_interval)
				
				for(c=0;c<50;c++){
					clearInterval(invaders_motion_interval[c])
					if(invaders[c].alive){
						id = "i" + c;
						start_invaders_timers(id);
					}
				}
				if (invaders_remaining == 0){
					show_game_over(win);
				}
			}
		}
	}
}

function collision_check_player_missile_bases(missile){
	var missile_rect =  missile.getBoundingClientRect();
	
	for(c=0;c<4;c++){
		id = "base" + c;
		base = document.getElementById(id);
		base_rect = base.getBoundingClientRect();

		if (base_rect.left < missile_rect.right && base_rect.right > missile_rect.left && base_rect.top < missile_rect.bottom && base_rect.bottom > missile_rect.top ){
			overlap = check_player_missile_overlap(missile_rect, base, base_rect);
			if (overlap){
				remove_player_missile(missile);
			}
		}
	}
} 

function check_player_missile_overlap(missile_rect, base, base_rect){
	var num = base.id.substring(4);
 	var index = parseInt(num);

	var x_overlap, y_overlap, sx, sy;
	
	if(missile_rect.left < base_rect.left){
		x_overlap = missile_rect.right - base_rect.left;
		sx = 0;
	}
	
	if(missile_rect.left >= base_rect.left && missile_rect.right <= base_rect.right){
		x_overlap = missile_rect.width;
		sx = missile_rect.left - base_rect.left;
	}
	
	if (missile_rect.right > base_rect.right){
		x_overlap = base_rect.right - missile_rect.left;
		sx = base_rect.right - x_overlap;
	}

	y_overlap = base_rect.bottom - missile_rect.top;
	
	if (y_overlap <= 2){
		sy = base_rect.height - y_overlap;
	}else{
		sy = base_rect.height - y_overlap;
		y_overlap = 2;
	}	
	
	var context = base.getContext("2d");
	var image_data = context.getImageData(sx, sy, x_overlap, y_overlap);
	
	let collision = false;
	var x, y;
	
	for(x=0; x<x_overlap;x++){
		for(y=0; y< y_overlap;y++){
			var colorIndices = getColorIndicesForCoord(x, y, x_overlap);

			var redIndex = colorIndices[0];
			var greenIndex = colorIndices[1];
			var blueIndex = colorIndices[2];
				
			var redForCoord = image_data.data[redIndex];
			var greenForCoord = image_data.data[greenIndex];
			var blueForCoord = image_data.data[blueIndex];
			
			if (redForCoord !=0 && greenForCoord != 0 && blueForCoord != 0){
				collision = true;
			}
		}
	}
	
	if (collision){
		context.putImageData(image_data, sx, sy);
		
		exp_screen_loc_top = missile_rect.top; // 11 is height of expl png img
		exp_screen_loc_left = missile_rect.left - 8; // 8 is about half width of expl png img
				
		var expl = document.createElement("img");
		var img = img_dir + "base_exp_inverted.png"
		expl.setAttribute("src", img);
		
		expl.setAttribute("class", "base_exp");
		expl.setAttribute("id", "base_exp" + bases[index].explosions_counter);
		
		expl.style.top = exp_screen_loc_top + "px";
		expl.style.left = exp_screen_loc_left + "px";
		expl.style.zIndex = "10";
		
		bases[index].explosions_counter++;
		var explosion = [5, expl.id];
		
		bases[index].explosions.push(explosion);
		
		document.getElementById("main").appendChild(expl);	
					
		var img_data = context.createImageData(10,10);
		context.putImageData(img_data, sx, sy-4);
		return true;
	}
	return false;
}

function remove_bases(){
	for(c=0;c<4;c++){
		base = document.getElementById("base" +c);
		parent = document.getElementById("main");
		parent.removeChild(base);
	}
}

function reset(){
	movedown = false;
	moveright = true;

	if (ship.missile_count > 0 ){
		for(c=0;c<ship.missile_count;c++){
			missile = document.getElementById(c);
			if(missile){
				remove_player_missile(missile);
			}
		}
	}
	
	for(c=0; c < max_invader_missiles; c++){
		if(invader_missiles[c]){
			missile = document.getElementById(c);
			if(missile){
				console.log("inv missile at reset =");
				console.log(missile); 
				remove_invader_missile(missile);
				invader_missiles[c] = false;
			}
		}
	}
	
	reset_invaders()
	
	ship.exploding = false;
	s = document.getElementById("ship");
	ship_img = img_dir + "ship.png";
	s.setAttribute("src", ship_img);
	s.style.left = 100 + "px";
}

function reset_invaders(){
	var left = screen_left;
	
	for(c=0; c < 5; c++){
		for(d=0; d < 10; d++){
			if (invaders[c*10 + d].alive){
				var id = "i" + (c*10 + d);
						
				clearInterval(invaders_motion_interval[c*10+d]);
			
				inv = document.getElementById(id);
				inv.style.left = left + "px";
			
				invader_img_counters[c*10+d] = 0;
				invader_img_timers[c*10+d] = 0;
				
				start_invaders_timers(id);
			}
			left += 45;
		}
	left = screen_left;
	}
}

function setup_game(){
	setup_invaders();
	setup_invader_missiles();
	setup_bases();
	add_key_handlers();
	
	var s = document.createElement("img");
	s.setAttribute("src", "images/ship.png");
	s.setAttribute("id", "ship");
	s.setAttribute("class", "ship");
	s.style.left = 100 + "px"
	s.style.top = screen_height - 16 + "px"
	
	document.getElementById("main").appendChild(s);
	
	ship = new Ship();
}

function setup_invaders(){
	
	var top = 10;
	var left = screen_left;
	var img;
	
	for(c=0; c < 5; c++){
		for(d=0;d<10; d++){
			var id_num = c*10 + d; 
			var id = "i" + id_num;
			//console.log(id);
			
			rule = "#" + id + " { position: absolute; top: "+ top + "px; left: " + left + "px; }"
			stylesheet.insertRule(rule);
			
			var inv = document.createElement("img");
			inv.setAttribute("id", id);
			inv.setAttribute("id_num", id_num) 
			inv.setAttribute("class", "inv");
			inv.setAttribute("position", "absolute")
			inv.style.left = left + "px";
			inv.style.top = top + "px";
			  
			if (id_num <10 ){
				img = img_dir + "top_invader0.png"
				inv.setAttribute("src", img);
			}
			
			else if (id_num >= 10 && id_num < 30){
				img = img_dir + "middle_invader0.png"
				inv.setAttribute("src", img);
			}
			
			else{
				img = img_dir + "bottom_invader0.png"
				inv.setAttribute("src", img);
			}
			
			document.getElementById("main").appendChild(inv);
		
			var invader = new Invader();
			invader.id = id;
			invader.id_num = id_num;
			invader.img = img
			invader.alive = true;		
			invaders.push(invader);
			
			invader_img_counters[c*10+d] = 0;
			invader_img_timers[c*10+d] = 0;
			left += 45;
		}
	top += 45;
	left = screen_left;
	}
}
	
function setup_invader_missiles(){
	for(c=0; c<max_invader_missiles; c++){
		invader_missiles[c] = false;
	}
}

function setup_bases(){
	
	var top = 460;
	var left = screen_left + 170;
	var base_img_src = img_dir + "base.png";
		
	for(c=0; c < 4; c++){
		var id= "base" + c;
		var base = document.createElement('canvas');
		
		let ctx = base.getContext("2d");
		
		var img = document.createElement("IMG");
		img.onload = function(){
			ctx.drawImage(img,0,0);
		};
		img.src = base_img_src;
			
		base.setAttribute("id", id);
		base.setAttribute("class", "base");
		base.setAttribute("position", "absolute");
		base.setAttribute("left", left + "px");
		base.setAttribute("top", top + "px");

		base.setAttribute("width", "60px");
		base.setAttribute("height", "44px");
	
		base.style.left = left + "px";
		base.style.top = top + "px;"

		var rule = "#" + id + " { position: absolute; top: "+ top + "px; left: " + left + "px; width: 60px; height: 44px; }"
		stylesheet.insertRule(rule);

		base_obj = new Base();
		bases.push(base_obj);
				
		document.getElementById("main").appendChild(base);
		left += 230;
	}
}

function show_game_over(type){
	for (missile of ship.missiles){
		remove_player_missile(missile);
	}
	
	for(c=0; c < invader_count; c++){
		clearInterval(invaders_motion_interval[c]);
	}
	
	for(c=0; c < max_invader_missiles; c++){
		if(invader_missiles[c]){
			clearInterval(invader_missiles_motion_interval[c]);
			clearInterval(invader_missiles_collision_interval[c]);
		
			missile = document.getElementById("" + c);
			remove_invader_missile(missile, c);
		}
	}

	clearInterval(firing_interval);
	clearInterval(explosions_interval);
	
	if (type == win){
		console.log("game over - you win" );
	}else{
		console.log("game over - you lose" );
	}
}

function getCssProperty(elmId, property){
	var elem = document.getElementById(elmId);
	return window.getComputedStyle(elem, null).getPropertyValue(property);
}

function add_key_handlers(){
	document.addEventListener('keydown', function(event) {
    	if (!game_over){
			if(event.keyCode == 37 || event.keyCode == 90) {
			    var left_str = getCssProperty("ship", "left");
			    var left = parseInt(left_str, 10);
				if(left > screen_left){
					left -= 5;
				}
				document.getElementById("ship").style.left = left + "px";
			}
			else if(event.keyCode == 39 || event.keyCode == 88) {
			    var left_str = getCssProperty("ship", "left");
			    var left = parseInt(left_str, 10);
				if(left < screen_right){
					left += 5;
				}
				document.getElementById("ship").style.left = left + "px";
    		}
    		else if(event.keyCode == 32){
    			fire_player_missile();
    		}
    	}
	});
}

function Invader(){
	this.id = "",
	this.left = 0,
	this.top = 0,
	this.alive = false,
	this.exploding = false,
	this.explosion_timer = 5
	}

function Missile(){
	this.id = "",
	this.left = 0,
	this.top = 0,
	this.alive = false
	}
	
function Base(){
	this.id = "",
	this.left = 0,
	this.top = 0,
	
	this.explosions_counter = 0,
	this.explosions = []
	}
	
function Ship(){
	this.left = 0,
	this.top = 0,
	this.lives = 3
	this.alive = true;
	this.exploding = false;
	this.img_timer = 5;
	this.exposion_image_counter = 0;
	this.missile_count = 0;
	this.missiles = []
	}


